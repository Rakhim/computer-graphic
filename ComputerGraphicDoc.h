// ComputerGraphicDoc.h : ��������� ������ CComputerGraphicDoc
//

#pragma once
#include "Offscreen.h"
#include "Triangle.h"

class CComputerGraphicDoc : public CDocument
{
protected: // ������� ������ �� ������������
	CComputerGraphicDoc();
	DECLARE_DYNCREATE(CComputerGraphicDoc)

// ��������
public:

// ��������
public:
  void write(Offscreen *offscreen) {
    Triangle *triangle = new Triangle(
      Vertex(100, 100, -100),
      Vertex(100, -100, 100),
      Vertex(-100, 100, -100),
      0x00EE00
    );
    offscreen->write(triangle);
  }
// ���������������
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
#ifdef SHARED_HANDLERS
	virtual void InitializeSearchContent();
	virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

// ����������
public:
	virtual ~CComputerGraphicDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// ��������� ������� ����� ���������
protected:
	DECLARE_MESSAGE_MAP()

#ifdef SHARED_HANDLERS
	// ��������������� �������, �������� ���������� ������ ��� ����������� ������
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS
};
