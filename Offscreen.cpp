#include "StdAfx.h"
#include "Offscreen.h"

Offscreen::Offscreen(CDC *pDC, size_t width, size_t height):
  nWidth(width),
  nHeight(height)
{
  cdcCompatible = new CDC();
  cdcCompatible->CreateCompatibleDC(pDC);

  cBitmap = new CBitmap();
  cBitmap->CreateCompatibleBitmap(pDC, nWidth, nHeight);

  cBitmapOld = cdcCompatible->SelectObject(cBitmap);
  cdcCompatible->PatBlt(0, 0, nWidth, nHeight, WHITENESS);
}

Offscreen::~Offscreen(void)
{
  delete cdcCompatible;
  delete cBitmap;  
}

void Offscreen::write(Printable *pPrintable) {
  pPrintable->print(cdcCompatible);
}

void Offscreen::print(CDC *pDC, LPRECT lpRect) {
  pDC->BitBlt(lpRect->left, lpRect->top, lpRect->right, lpRect->bottom, cdcCompatible, 0, 0, SRCCOPY);
}

void Offscreen::stretch(CDC *pDC, LPRECT lpRect, LPRECT lpNewRect) {
  pDC->StretchBlt(
      lpRect->left, lpRect->top, lpRect->right, lpRect->bottom,
      cdcCompatible,
      lpNewRect->left, lpNewRect->top, lpNewRect->right, lpNewRect->bottom,
      SRCCOPY);
}