
// ComputerGraphicView.h : ��������� ������ CComputerGraphicView
//

#pragma once
#include "Offscreen.h"

class CComputerGraphicView : public CView
{
protected: // ������� ������ �� ������������
	CComputerGraphicView();
	DECLARE_DYNCREATE(CComputerGraphicView)
  
// ��������
public:
	CComputerGraphicDoc* GetDocument() const;
  Offscreen *pOffscreen;

// ��������
public:

// ���������������
public:
	virtual void OnDraw(CDC* pDC);  // �������������� ��� ��������� ����� �������������
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:

// ����������
public:
	virtual ~CComputerGraphicView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// ��������� ������� ����� ���������
protected:
	DECLARE_MESSAGE_MAP()
public:
  afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
  afx_msg void OnDestroy();
  virtual void OnInitialUpdate();
  afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
};

#ifndef _DEBUG  // ���������� ������ � ComputerGraphicView.cpp
inline CComputerGraphicDoc* CComputerGraphicView::GetDocument() const
   { return reinterpret_cast<CComputerGraphicDoc*>(m_pDocument); }
#endif

