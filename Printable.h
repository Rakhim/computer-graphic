#pragma once

class Printable
{
public:
  virtual void print(CDC *pDC) = 0;
  Printable(void);
  virtual ~Printable(void);
};

