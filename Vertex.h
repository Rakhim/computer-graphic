#pragma once

class Vertex
{
public:
  double x;
  double y;
  double z;

public:
  Vertex(double _x, double _y, double _z):
    x(_x), y(_y), z(_z)
  {
  }

  ~Vertex(void) {
  }

  Vertex operator- (const Vertex &_vertex) const {
    return Vertex(x - _vertex.x, y - _vertex.y, z - _vertex.z);
  }
  
  Vertex operator+ (const Vertex &_vertex) const {
    return Vertex(x + _vertex.x, y + _vertex.y, z + _vertex.z);
  }

  double operator* (const Vertex &_vertex) const {
    return x * _vertex.x + y * _vertex.y + z * _vertex.z;
  }
};

