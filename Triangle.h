#pragma once
#include "Vertex.h"
#include "Printable.h"
#include <iostream>
//#include "Transformable.h"

class Triangle:
  public Printable
//,  public Transformable
{
public:
  Vertex v1;
  Vertex v2;
  Vertex v3;
  int color;

public:
  Triangle(const Vertex &_v1, const Vertex &_v2, const Vertex &_v3, int _color):
    v1(_v1),
    v2(_v2),
    v3(_v3),
    color(_color)
  {
  }

  ~Triangle(void) {
  }

  void print(CDC *pDC) {
    printf("Triangle::print\n");
    normal();

    // ������������� �� ��� y.
    // v1.y <= v2.y <= v3.y
    if (v2.y < v1.y) {
      std::swap(v1.y, v2.y);
      std::swap(v1.x, v2.x);
    }

    if (v3.y < v1.y) {
      std::swap(v1.y, v3.y);
      std::swap(v1.x, v3.x);
    }

    if (v2.y > v3.y) {
      std::swap(v2.y, v3.y);
      std::swap(v2.x, v3.x); 
    }
    
    int xmove = 0;
    int ymove = 0;
    
    //int most_left_x = min(min(v1->x, v2->x), v3->x);
    //int most_right_x = max(max(v1->x, v2->x), v3->x);
    double left_x;
    double right_x;
    int i;
    int j;

    double dx31 = v3.x - v1.x;
    double dx21 = v2.x - v1.x;
    double dy21 = v2.y - v1.y;
    double dy31 = v3.y - v1.y;

    double dx21_dy21 = dx21 / dy21;
    double dx31_dy31 = dx31 / dy31;

    for (j = v1.y; j < v2.y; j++) {
      left_x = (j - v1.y) * dx21_dy21 + v1.x;
      right_x = (j - v1.y) * dx31_dy31 + v1.x;
      if (left_x > right_x) {
        std::swap(left_x, right_x);
      }

      for (i = left_x; i <= right_x; i++) {
      pDC->SetPixel(xmove + i, ymove + j, color * std::cos((double)0.100124 * i * j));
      }
    }

    double dx32 = v3.x - v2.x;
    double dy32 = v3.y - v2.y;

    double dx32_dy32 = dx32 / dy32;

    for (j = v2.y; j < v3.y; j++) {
      left_x = (j - v2.y) * dx32_dy32 + v2.x;
      right_x = (j - v1.y) * dx31_dy31 + v1.x;

      if (left_x > right_x) std::swap(left_x, right_x);
      for (i = left_x; i <= right_x; i++) {
        pDC->SetPixel(xmove + i, ymove + j, color * std::cos((double)0.000124 * i * i));
      }
    }
  }

  double getNumber(Vertex *sun) const {
    
    double center_x = (v1.x + v2.x + v3.x);
    double center_y = (v1.y + v2.y + v3.y);
    double center_z = (v1.z + v2.z + v3.z);

    //printf("center(%5.5f, %5.5f, %5.5f) %#010x \n", v1->x, center_y, center_z, color);
    return (center_x * sun->x + center_y * sun->y + center_z * sun->z);  
  }
  
  Vertex normal() {
    return Vertex(
      (v2.y - v1.y) * (v3.z - v1.z) - (v2.z - v1.z) * (v3.y - v1.y),
      (v2.z - v1.z) * (v3.x - v1.x) - (v2.x - v1.x) * (v3.z - v1.z),
      (v2.x - v1.x) * (v3.y - v1.y) - (v2.y - v1.y) * (v3.x - v1.x)
    );
  }
/*
  Triangle transform(Matrix3D *_transformation) {
    return Triangle(
      _transformation->transform(v1),
      _transformation->transform(v2),
      _transformation->transform(v3),
      color
    );
  }*/
};