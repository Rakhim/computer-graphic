
// ComputerGraphicView.cpp : ���������� ������ CComputerGraphicView
//

#include "stdafx.h"
// SHARED_HANDLERS ����� ���������� � ������������ �������� ��������� ���������� ������� ATL, �������
// � ������; ��������� ��������� ������������ ��� ��������� � ������ �������.
#ifndef SHARED_HANDLERS
#include "ComputerGraphic.h"
#endif

#include "ComputerGraphicDoc.h"
#include "ComputerGraphicView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CComputerGraphicView

IMPLEMENT_DYNCREATE(CComputerGraphicView, CView)

BEGIN_MESSAGE_MAP(CComputerGraphicView, CView)
  ON_WM_CREATE()
  ON_WM_DESTROY()
  ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

// ��������/����������� CComputerGraphicView

CComputerGraphicView::CComputerGraphicView()
{
	// TODO: �������� ��� ��������

}

CComputerGraphicView::~CComputerGraphicView()
{
}

BOOL CComputerGraphicView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �������� ����� Window ��� ����� ����������� ���������
	//  CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// ��������� CComputerGraphicView

void CComputerGraphicView::OnDraw(CDC* pDC)
{
	CComputerGraphicDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: �������� ����� ��� ��������� ��� ����������� ������
  RECT r;
  GetClientRect(&r);
  pOffscreen->print(pDC, &r);
}


// ����������� CComputerGraphicView

#ifdef _DEBUG
void CComputerGraphicView::AssertValid() const
{
	CView::AssertValid();
}

void CComputerGraphicView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CComputerGraphicDoc* CComputerGraphicView::GetDocument() const // �������� ������������ ������
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CComputerGraphicDoc)));
	return (CComputerGraphicDoc*)m_pDocument;
}
#endif //_DEBUG


// ����������� ��������� CComputerGraphicView


int CComputerGraphicView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
  if (CView::OnCreate(lpCreateStruct) == -1)
    return -1;

  // TODO:  
  int cx = GetSystemMetrics(SM_CXFULLSCREEN);
  int cy = GetSystemMetrics(SM_CYFULLSCREEN);
  pOffscreen = new Offscreen(this->GetDC(), cx, cy);

  return 0;
}


void CComputerGraphicView::OnDestroy()
{
  CView::OnDestroy();

  // TODO: 
  delete pOffscreen;
}


void CComputerGraphicView::OnInitialUpdate()
{
  CView::OnInitialUpdate();
  CComputerGraphicDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;
  // TODO: 
  pDoc->write(pOffscreen);
}


void CComputerGraphicView::OnLButtonDown(UINT nFlags, CPoint point)
{
  // 
  printf("CComputerGraphicView::OnLButtonDown\n");

  CView::OnLButtonDown(nFlags, point);
}
