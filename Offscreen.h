#pragma once

#include "Printable.h"

class Offscreen
{
  CDC *cdcCompatible;
  CBitmap *cBitmap;
  CBitmap *cBitmapOld;
  int nWidth;
  int nHeight;

public:
  Offscreen(CDC *pDC, size_t nWidth, size_t nHeight);
  ~Offscreen(void);
  
  void write(Printable *pPrintable);
  void print(CDC *pDC, LPRECT lpRect);
  void Offscreen::stretch(CDC *pDC, LPRECT lpRect, LPRECT lpNewRect);
};
